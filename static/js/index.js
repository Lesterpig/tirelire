import * as ui from './display.js'
import config from './config.js'
import FormExpense from './form-expense.js'
import Store from './store.js'

const registerSW = async () => {
  const registration = await navigator.serviceWorker.getRegistration()
  if (!registration || !navigator.serviceWorker.controller) {
    await navigator.serviceWorker.register(`${config.baseURL}/sw.js`)
  }
}

window.onload = async () => {
  const store = new Store()
  const formExpense = new FormExpense(store)
  ui.events.onEdit = async (e) => {
    await formExpense.open()
    await formExpense.populate(e)
  }

  document.onkeydown = (e) => {
    e = e || window.event
    if (e.keyCode === 27) { // escape
      $('#modal-expense').checked = false
    }
  }

  $$('.modal').forEach((m) => m.classList.remove('hidden'))
  $('#modal-expense').checked = false
  $('#add-expense').onclick = ({ target }) => formExpense.open({ focus: true })
  $('#open-report').onclick = ({ target }) => ui.openReport(store)
  $('#open-misc').onclick = ({ target }) => ui.toggleMisc(store)
  $('#close-report').onclick = ({ target }) => ui.toggleReport(store)
  $('#close-misc').onclick = ({ target }) => ui.toggleMisc(store)
  $('#ledger-submit').onclick = async () => {
    const nameEl = $('#ledger-name')
    if (!nameEl.value) {
      return
    }

    const l = Store.newLedger(nameEl.value)
    await store.addLedger(l)
    await ui.displayLedgers(store)
    nameEl.value = ''
    document.getElementById(`tab-${l.public}`).click()
  }
  $('#ledger-erase').onclick = async () => {
    ui.toggleMisc()
    const ledger = ui.currentLedger()
    await store.rmLedger(ledger)
    await ui.displayLedgers(store)
    $('#ledgers-list input').click()
  }
  $('#sync').onclick = async ({ target }) => {
    target.setAttribute('class', 'btn-small')

    const ledger = ui.currentLedger()

    try {
      await store.push(ledger)
      await store.pull(ledger)
      await ui.displayEntries(store, ledger)
      target.classList.add('btn-success')
    } catch (e) {
      console.error(e)
      target.classList.add('btn-danger')
    }

    target.classList.remove('disabled')
  }

  try {
    await Promise.all([
      registerSW(),
      ui.displayLedgers(store, ui.events.onEdit),
    ])
  } catch (e) {
    $('#unsupported').setAttribute('class', '')
  }

  $('#ledgers-list input').click()

  const hash = window.location.hash
  if (hash) {
    try {
      const l = Store.decodeLedger(hash.slice(1))
      await store.addLedger(l)
      await ui.displayLedgers(store)
      document.getElementById(`tab-${l.public}`).click()
      $('#sync').click()
    } catch (e) {
      console.warn(e)
    }
  }
}
